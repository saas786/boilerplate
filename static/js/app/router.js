define ([
    'jquery',
    'underscore',
    'backbone',

    // Views here
    'views/projects/list',
    'views/users/list'
], function (jQuery, _, Backbone, projectListView, userListView) {

    var AppRouter = Backbone.Router.extend({
        
        routes: {
            'projects': 'showProjects',
            'users': 'showUsers',
            
            // Default route handler
            '*actions': 'defaultAction'
        },

        // View Function Renders
        
        showProjects: function (){
            projectListView.render(); 
        },

        showUsers: function (){
            userListView.render(); 
        },

        defaultAction: function (actions) {
            console.log('Route not found', actions);
        }
    });

    var initialize = function () {
        var app_router = new AppRouter;
        Backbone.history.start();
    };
    
    return {
        initialize: initialize
    };
});
