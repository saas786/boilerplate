// THIS IS JUST AN EXAMPLE VIEW
define([
    'jquery',
    'underscore',
    'backbone'

    // Templates should go here!
    // 'text!templates/users/list.html'

], function(jQuery, _, Backbone) {
    var usersListView = Backbone.View.extend({
        render: function() {
            alert('RENDER THE USER LIST VIEW!');
        }
    });

    return new usersListView({});
});
