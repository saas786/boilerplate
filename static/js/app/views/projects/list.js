// THIS IS JUST AN EXAMPLE VIEW
define([
    'jquery',
    'underscore',
    'backbone'

    // Templates should go here!
    // 'text!templates/projects/list.html'

], function(jQuery, _, Backbone) {
    var projectListView = Backbone.View.extend({
        render: function() {
            alert('RENDER THE PROJECT LIST VIEW!');
        }
    });

    return new projectListView({});
});
