define([
    'jquery',
    'underscore',
    'backbone',
    'router',
], function(jQuery, _, Backbone, Router) {
    return {
        initialize: function() {
            Router.initialize(); 
        }
    };
});
