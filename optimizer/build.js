var requirejs = require('requirejs'),
    fs = require('fs');

var config = {
    paths: {
        // Libs
        jquery: 'libs/jquery',
        underscore: 'libs/underscore',
        backbone: 'libs/backbone',
        bootstrap: 'libs/bootstrap',
    
        // App
        app: 'app/app',
        router: 'app/router',
            
        // Backbone directory definitions
        models: 'app/models',
        collections: 'app/collections',
        views: 'app/views'

    },
    baseUrl: '../static/js',
    name: 'main',
    out: '../static/js/main.min.js'
};

requirejs.optimize(config, function (buildResponse) {
    var contents = fs.readFileSync(config.out, 'utf8');
});
